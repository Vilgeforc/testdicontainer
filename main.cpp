#include <QCoreApplication>
#include "DiContainer.h"
#include <QDebug>
#include "TestClases.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    {
        DiContainer container;

        container.registerFactory<SingleDep, DiContainer::SingleInstance>(&createFunction);
        container.registerFactory<MultyDep>(&createFunction);
        container.registerFactory<Test>(&createFunction<SingleDep, MultyDep>);

        QSharedPointer<Test> test1 = container.resolve<Test>();

        qDebug()<<"test1 initital - "<<test1->toString();
        test1->mDep()->n = 111;
        test1->sDep()->n = 222;
        qDebug()<<"test1 modified - "<<test1->toString();

        {
            QSharedPointer<Test> test2 = container.resolve<Test>();
            qDebug()<<"test2 initital - "<<test2->toString();
            test2->mDep()->n = 666;
            test2->sDep()->n = 777;
            qDebug()<<"test2 modified - "<<test2->toString();
            qDebug()<<"test1 - "<<test1->toString();
        }

        QSharedPointer<Test> test3 = container.resolve<Test>();
        qDebug()<<"test3 initital - "<<test3->toString();
    }

    return a.exec();
}
