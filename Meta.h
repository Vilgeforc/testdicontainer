#ifndef META_H
#define META_H
#include <QString>

template<class classT>
QString className();

#define REGISTER_NAME(name) \
    template<>\
    QString className<name>()\
    {\
        return QString(#name);\
    }

#endif // META_H
