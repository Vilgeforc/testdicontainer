#ifndef DICONTAINER_H
#define DICONTAINER_H
#include <QHash>
#include "Factory.h"
#include "Meta.h"

class DiContainer
{
public:

    enum RegistrationType
    {
        SingleInstance,
        UnmanadgesMultyInstance
    };

    template<class classT, RegistrationType type>
    void registerFactory(typename Factory<classT>::creationFunction f)
    {
        m_factories.insert(className<classT>(), QSharedPointer<IFactory>(new Factory<classT>(f, type == SingleInstance)));
    }

    template<class classT>
    void registerFactory(typename Factory<classT>::creationFunction f)
    {
        m_factories.insert(className<classT>(), QSharedPointer<IFactory>(new Factory<classT>(f)));
    }

    template<class classT>
    QSharedPointer<classT> resolve() const
    {
        Factory<classT> * factory = static_cast<Factory<classT>* >(m_factories[className<classT>()].data());
        return factory->create(*this);
    }


private:
    QHash<QString, QSharedPointer<IFactory> > m_factories;
};


template<class classT>
classT* createFunction(const DiContainer& c)
{
    Q_UNUSED(c);
    return new classT();
}

template<class depT, class classT>
classT* createFunction(const DiContainer& c)
{
    return new classT(c.resolve<depT>());
}

template<class depT1, class depT2, class classT>
classT* createFunction(const DiContainer& c)
{
    return new classT(c.resolve<depT1>(), c.resolve<depT2>());
}


#endif // DICONTAINER_H
