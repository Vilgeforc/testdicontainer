#ifndef TESTCLASES_H
#define TESTCLASES_H
#include <QSharedPointer>
#include "Meta.h"

class SingleDep
{
public:
    SingleDep() : n(1){}
    int n;
};
REGISTER_NAME(SingleDep)

class MultyDep
{
public:
    MultyDep() : n(2){}
    int n;
};
REGISTER_NAME(MultyDep)

class Test
{
public:
    Test(QSharedPointer<SingleDep> sDep, QSharedPointer<MultyDep> mDep)
        : m_sDep(sDep), m_mDep(mDep)  {}

    QString toString() const
    {
        return QString("single : %1 - multy : %2").arg(m_sDep->n).arg(m_mDep->n);
    }

    QSharedPointer<SingleDep> sDep() const
    {
        return m_sDep;
    }

    QSharedPointer<MultyDep> mDep() const
    {
        return m_mDep;
    }

    private:
    QSharedPointer<SingleDep> m_sDep;
    QSharedPointer<MultyDep> m_mDep;
};
REGISTER_NAME(Test)

#endif // TESTCLASES_H
