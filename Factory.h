#ifndef FACTORY_H
#define FACTORY_H
#include <QSharedPointer>
#include <QDebug>

#include "IFactory.h"
#include "Meta.h"

class DiContainer;

template<class resultT>
class Factory : public IFactory
{
public:
    typedef resultT* (*creationFunction)(const DiContainer& container);
    Factory(creationFunction f, bool isSingle = false): m_function(f), m_isSingleInstance(isSingle){}

    QSharedPointer<resultT> create(const DiContainer& container)
    {
        if (!m_isSingleInstance)
            return QSharedPointer<resultT>(m_function(container));
        if (m_data.isNull())
            m_data = QSharedPointer<resultT>(QSharedPointer<resultT>(m_function(container)));
        return m_data;
    }

    ~Factory() {qDebug()<<QString("Factory fo type %1 destroyed.").arg(className<resultT>());}
private:
    creationFunction m_function;
    bool m_isSingleInstance;
    QSharedPointer<resultT> m_data;
};

#endif // FACTORY_H
